@extends('layouts.frontend.master')
@section('title', trans('frontend.shopist_home_title') .' < '. get_site_title() )

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    </head>
<body>  
<div class="col-md-12">
    <div class="modal-header text-center">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2">PLEASE CHOOSE ANY ONPTIONS YOU WANT TO VIEW</h4>
    </div><br><br>                         
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-4 col-sm-12 d-flex justify-content-center">                                
            <a href="{{ url('shop') }}">
            <div class="text-center student-block d-flex justify-content-center align-items-center">
                <div> <button type="button" class="btn btn-info">EXPRESS ORDER</button>                                                                                
            </div>
            </a>                                        
        </div>
    </div>
    <div class="col-md-4 col-sm-12 d-flex justify-content-center">
        <a href="{{ url('shop') }}">
        <div class="text-center student-block d-flex justify-content-center align-items-center">
            <div>
                <button type="button" class="btn btn-info">NORMAL ORDER</button>
            </div>
        </div>
        </a>
    </div>
</div>
@endsection

</body>
</html>

 

 